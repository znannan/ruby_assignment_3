require_relative '../lib/seller_name.rb'
RSpec.describe Seller_name do 
    # normal seller
    seller_name_html_block_0 = '<h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold">            <a href="/gp/aag/main/ref=olp_merch_name_3?ie=UTF8&amp;asin=0262510871&amp;isAmazonFulfilled=0&amp;seller=AQ29OYITDFRJC">Book-Buzz</a>        </span></h3>'
    # amazon 
    seller_name_html_block_1 = '<h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif"></h3>'
    describe ".parse_seller_name" do
        it "should get normal seller name" do
            seller_name = Seller_name.parse(seller_name_html_block_0)
            expect(seller_name).to eq("Book-Buzz")
        end

        it "should get Amazon name" do
            seller_name = Seller_name.parse(seller_name_html_block_1)
            expect(seller_name).to eq("Amazon")
        end
       
    end

end