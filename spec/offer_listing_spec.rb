require_relative '../lib/offer-listing.rb'
RSpec.describe Offer_listing do 
    
    offer_listing_html_block = File.read(File.join(File.dirname(__FILE__),'amz_example_page_source.html'))
    
    describe ".parse offer-listing" do
        
        it "should get all sellers" do
            offers = Offer_listing.parse(offer_listing_html_block)
            expect(offers.size).to eq(10)
        end

        
    end
end