require 'nokogiri'
require_relative  './price.rb'
require_relative  './condition.rb'
require_relative  './seller_name.rb'
require_relative  './seller_rating.rb'
class Offer_listing
    def initilaize(offer_listing_html)
        @offer_listing_html = offer_listing_html
    end

    def self.parse(offer_listing_html)
        doc = Nokogiri::HTML(offer_listing_html)
        #取得所有seller的 div
        sellers_html = doc.css('//div.olpOffer')
        #sellers_doc = doc.xpath("//div[@class='.olpOffer']")
        sellers_arr = Array.new
        sellers_html.each do |seller_html|
            seller_name = Seller_name.parse seller_html
            price = Price.parse_sale_price seller_html
            shipping_fee = Price.parse_shipping_fee seller_html
            condition = Condition.parse seller_html
            seller_rating = Seller_rating.parse seller_html
            seller_arr = Array.new
            seller_arr['seller_name'] = seller_name
            seller_arr['price'] = price
            seller_arr['shipping_fee'] = shipping_fee
            seller_arr['condition'] = condition
            seller_arr['seller_rating'] = seller_rating
            sellers_arr << seller_arr
            puts sellers_arr
        end
    end

end






